// text.h by lzm @ 4 march 2005

#include <gl/gl.h>
#include <gl/glu.h> // mipmap
#include <stdio.h>  // file
#include <stdarg.h> // va_list

class text {
  private:
    char *d;
    unsigned int id;
    int w, h, s;
    float sx, sy;

    void bmp ();
    void quad (int x, int y, float x0, float y0);

  public:
    text () { sx = 1.0f/32; sy = 1.0f/3; }
    ~text () { }

    void load ();
    void size (int w_, int h_) { w = w_; h = h_; }
    void set ();
    void draw (int x, int y, char *t, ...);
};

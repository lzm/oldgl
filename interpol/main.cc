// game by lzm @ 2 march 2005 14:25 -300

#include "win.h"
#include "text.h"
#include "draw.h"

void size (int w, int h);
void mouse (int x, int y);
void click (int b, int x, int y);

void polys ();
void count ();

win wnd;
text tex;
draw eng;

int main (HINSTANCE, HINSTANCE, char*, int)
{
    wnd.cria ("game");

    wnd.size = &size;
    wnd.mouse = &mouse;
    wnd.click = &click;

    wnd.show (400, 400);

    eng.init ();
    tex.load ();
    
    while (wnd.peek ())
    {
        eng.set ();
        eng.cls ();
        eng.drw ();
        tex.set ();
        count ();
        polys ();
        wnd.swap ();
    }

    return 0;
}  

void size (int w, int h) { tex.size (w, h); eng.size (w, h); }
void mouse (int x, int y) { eng.mouse (x, y); }
void click (int b, int x, int y) { eng.click (b, x, y); }

int de (float f, int p) { int r = (int)((f-(int)f)*p); return r<0?-r:r; }
void polys ()
{
    for (int i = 0; i < eng.polyn; i++)
      tex.draw (-190, 10+10*i, "%4d.%03d %5d.%03d",
        (int)eng.poly[i].x, de(eng.poly[i].x,1000),
        (int)eng.poly[i].y, de(eng.poly[i].y,1000));
}

unsigned int h[9] = { 1, 1, 1, 1, 1, 1 };
void count ()
{
    unsigned int k = GetTickCount ();
    float f = 5000.0f/(k-h[h[0]]);
    h[h[0]] = k; h[0] = (h[0]%8)+1;
    tex.draw (-80, -20, "%2d.%02dhz", (int)f, de(f,100));
}

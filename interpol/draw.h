#include <gl/gl.h>
#include <gl/glu.h> // persperctive
#include <math.h>   // pow
#include "vector.h"

class draw {
  private:
    int w, h;

  public:
    draw () { }
    ~draw () { }

    _v poly[10];
    int polyn;

    void size (int w_, int h_);
    void init ();
    void cls ();
    void set ();
    void drw ();

    void mouse (int x, int y);
    void click (int b, int x, int y);
};

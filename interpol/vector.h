// vector

#include <string.h> // memcpy

struct _v {
  float x, y;
  ~_v (){ }
  _v (float x_ = 0, float y_ = 0){ x = x_; y = y_; }
  _v operator+(_v v){ return _v(x+v.x, y+v.y); }
  _v operator-(_v v){ return _v(x-v.x, y-v.y); }
  _v operator*(_v v){ return _v(x*v.x, y*v.y); }
  _v operator/(_v v){ return _v(x/v.x, y/v.y); }
  _v operator*(float n){ return _v(x*n, y*n); }
  _v operator/(float n){ return _v(x/n, y/n); }
};

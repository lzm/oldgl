#include "draw.h"

void draw::size (int w_, int h_) { w = w_; h = h_; glViewport (0, 0, w, h); }

void draw::init () { glClearColor (0.0f, 0.0f, 0.0f, 1.0f); }

void draw::cls () { glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); }

void draw::set ()
{
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    glOrtho (-w/2, w/2, h/2, -h/2, -1, 1);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();
    
    glDisable (GL_BLEND);
    glDisable (GL_TEXTURE_2D);
    glDisable (GL_SCISSOR_TEST);
    glEnable (GL_DEPTH_TEST);
}

int s = 7;
_v p[8];
int pn = 0;
bool line = true;

_v _v_ (float f) { return _v (f, f); }

_v det(_v *u, int s)
{
    if (s == 3) return u[0]*u[4]*u[8]+u[1]*u[5]*u[6]+u[2]*u[3]*u[7]-
                       u[2]*u[4]*u[6]-u[1]*u[3]*u[8]-u[0]*u[5]*u[7];
    if (s == 2) return u[0]*u[3]-u[1]*u[2];
    if (s == 1) return u[0];

    int h = s-1, i, x, y, z;
    _v d = _v(0, 0), v[s][s], o[h*h];

    memcpy (v, u, sizeof v);
    for (i = 0; i < s; i++)
    {
        z = 0;
        for (y = 1; y < s; y++)
          for (x = 0; x < s; x++) 
            if (x-i) o[z++] = v[y][x];
        z = (i%2?-1:1);
        if (v[0][i].x == 0 && v[0][i].y == 0) continue;
        d = d+(v[0][i]*det(o, h)*z);
    }
    return d;
}

void quad (_v v, int sz, int c)
{
    glBegin (GL_QUADS);
      glColor3f ( 1, 1*c, 1*c); glVertex2f (v.x, v.y-sz);
      glColor3f (.5,.5*c,.5*c); glVertex2f (v.x-sz, v.y);
      glColor3f (.6,.6*c,.6*c); glVertex2f (v.x, v.y+sz);
      glColor3f (.5,.5*c,.5*c); glVertex2f (v.x+sz, v.y);
    glEnd ();
}

void draw::drw ()
{
    int n, i, x, y;

    for (i = 0; i < s+1; i++)
      quad (p[i], i<pn||pn==0?7:5, i<pn||pn==0?0:1);

    _v re = p[pn];
    if (pn) p[pn] = p[s];
    n = (pn?pn+1:s);
    _v m[n+1][n][n];

    for (i = 1; i < n+1; i++)
      for (y = 0; y < n; y++)
        for (x = 0; x < n; x++)
          m[i][y][x]=x==i&&i<n?p[y]:_v_(pow(y,x));
 
    _v d = det(*m[n],n), r[n];
    for (i = 1; i < n; i++) { r[i] = det(*m[i],n)/d; poly[i] = r[i]; }
    poly[0] = r[0] = p[0];
    polyn = n;

    if (line) glBegin (GL_LINE_STRIP);
    float t = -1.0f, ti = 1/10.0f; 
    while ((t += ti) < n) {
        d = _v(0, 0);
        for (i = 0; i < n; i++) d = d+r[i]*pow (t, i);
        if (line) {
          glColor3f((sin(6.28*t)+1.5)/2, 0, 0);
          glVertex2f(d.x, d.y);
        }
        else quad (d, 4, 1);
    }
    if (line) glEnd ();
    p[pn] = re;
}

void draw::mouse (int x, int y) { p[s] = _v (x-w/2, y-h/2); }
void draw::click (int b, int x, int y)
{
    switch (b) {
      case 1: p[pn] = _v (x-w/2, y-h/2); pn = (pn+1)%s; break;
      case 2: s = (s%7)+1; break;
      case 3: line = line?false:true; break;
    }
}

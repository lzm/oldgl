#include "text.h"

void text::bmp ()
{
    FILE *f = fopen("font.dat", "r+bt");
    d = new char[320*30];
    fread (d, 1, 320*30, f);
    fclose(f);

    gluBuild2DMipmaps (0x0DE1, 1, 320, 30, GL_RED, 0x1401, d);
    delete [] d;
}

void text::load ()
{
    glGenTextures (1, &id);
    glBindTexture (0x0DE1, id);
    glPixelStorei (0x0CF5, 1);
    glTexEnvf (0x2300, 0x2200, 0x2100);
    glTexParameteri (0x0DE1, 0x2800, 0x2601);
    glTexParameteri (0x0DE1, 0x2801, 0x2601);
    glTexParameteri (0x0DE1, 0x2802, 0x2901);
    glTexParameteri (0x0DE1, 0x2803, 0x2901);

    bmp ();
}

void text::set ()
{
    glDisable (GL_DEPTH_TEST);
    glEnable (GL_TEXTURE_2D);
    glEnable (GL_SCISSOR_TEST);
    glEnable (GL_BLEND);
    glBlendFunc (0x307, 0x301); // 0x1/0x307, 0x301

    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    glOrtho (0, w, 0, h, -1, 1);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();

    glBindTexture (GL_TEXTURE_2D, id);
}

void text::quad (int x, int y, float x0, float y0)
{
    float x1 = x0+sx, y1 = y0+sy,
          wi[3] = { 1.0f, 1.0f, 1.0f },
          gr[3] = { 0.5f, 0.5f, 0.5f };
    glScissor (x, y, x+s, y+s);
    glBegin (GL_QUADS);
      glColor3fv(wi); glTexCoord2f(x0, y1); glVertex2i(x, y+s);
      glColor3fv(gr); glTexCoord2f(x0, y0); glVertex2i(x, y);
      glColor3fv(gr); glTexCoord2f(x1, y0); glVertex2i(x+s, y);
      glColor3fv(wi); glTexCoord2f(x1, y1); glVertex2i(x+s, y+s);
    glEnd ();
}

void text::draw (int x, int y, char *t, ...)
{
    va_list v;
    char f[128];
    int sz;

    va_start (v, t);
    sz = _vsnprintf (f, 128, t, v);
    va_end (v);

    if (y < 0) y += h;
    if (x < 0) x += w;

    s = 10;

    for (int i = 0; i < sz; i++)
    {
      char c = f[i]-32, co = c%32, ro = (95-c)/32;
      if (c < 1 || c > 95) continue;
      quad (x+s*i, y, co*sx, ro*sy);
    }
}

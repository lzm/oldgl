// window.h by lzm @ 2 march 2005
// opengl window class

#include <windows.h>
#include <stdio.h>

class window {
  private:
   char name[16];
  public:
   HINSTANCE hi;
   WNDCLASS wc;
   HWND hw;
   HDC dc;
   HGLRC rc;
    
   void (*size)(int, int);
   void (*mouse)(int, int);
   void (*click)(int, int, int, int);
   void (*keyd)(int);
   void (*keyu)(int);

   window ();
   ~window ();

   void cria (char *n);
   void reg (char *n);
   void wnd ();
   void show (int w, int h);
   void pixel ();
   void free ();
   bool peek ();
   void swap ();
   static LRESULT _stdcall wndproc (HWND hw, UINT msg, WPARAM wp, LPARAM lp);

   void window::title (char *f, ...);
};

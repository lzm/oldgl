#include <string.h>
#include <math.h>
#include "draw.h"

float t = 0, r[4][4] = {{1,0,0,0},{0,.8,.5,0},{0,-.5,.8,0},{0,0,0,1}};
int ox, oy, p = 1, q = 0, o = 1, u = 1, w = 0;

void fire (int a, int b, int, int) { if (b) p = a?p?0:1:p; else q = a; }

void mult (float a[4][4], float b[4][4], float *d)
{
   float c[4][4];
   for (int i = 0; i < 4; i++) for (int j = 0; j < 4; j++)
      c[i][j] = a[0][j]*b[i][0]+a[1][j]*b[i][1]+a[2][j]*b[i][2]+a[3][j]*b[i][3];
   memcpy (d, c, 64);
}

void move (int x, int y)
{
   float dx = (x-ox)*0.01, dy = (y-oy)*0.01, cx, sx, cy, sy; ox = x; oy = y;
   if (!q) return; cx = cosf (dx); sx = sinf (dx); cy = cosf (dy); sy = sinf (dy);
   float r1[4][4] = {{cx,0,-sx,0},{0,1,0,0},{sx,0,cx,0},{0,0,0,1}}; // eixo y
   float r2[4][4] = {{1,0,0,0},{0,cy,sy,0},{0,-sy,cy,0},{0,0,0,1}}; // eixo x
   mult (r1, r, &r[0][0]); mult (r2, r, &r[0][0]);
}

void keyb (int k)
{
   if (k=='1') w = w?0:1; if (k=='2') o = o?0:1; if (k=='3') u = u?0:1;
   if (u) { glEnable (GL_DEPTH_TEST); glEnable (GL_LINE_SMOOTH); }
   else { glDisable (GL_DEPTH_TEST); glDisable (GL_LINE_SMOOTH); }
}

void draw (void)
{
   glClear (GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
   glLoadIdentity ();
   glTranslatef (0, -1, -10);
   glMultMatrixf ((GLfloat*)r);
   glRotatef (t, 0, 1, 0);
   t = t+(p?1.0f:0.04f);

   /*
      elips�ide                 hiperbol�ide             parabol�ide
      x� +y� +z� = 1            x� -y� +z� = 1           x� -y  +z� = 1
      y = sqrt ( -x� -z� +1 )   y = sqrt ( x� +z� -1 )   y = x� +z� +1
   */

   #define m 30
   struct vec { float x, y, z; } y[m][m][2];
   float a, b, c;

   for (int i = 0; i < m; i++)
      for (int j = 0; j < m; j++)
      {
         y[i][j][0].x = y[i][j][1].x = a = (i-m*0.5)*0.2;
         y[i][j][0].z = y[i][j][1].z = b = (j-m*0.5)*0.2;
         c = a*a*cosf (t*0.03) + b*b*sinf (t*0.03) - 2*sinf (t*0.1);
         c = w?c:c>0?sqrtf(c):o?0:-sqrtf(-c);
         y[i][j][0].y = c;
         y[i][j][1].y = -c;
      }

   #define abs(x) x>0?x:-x
   #define put(a,b,c) k = y[i+a][j+b][c]; \
                      glColor3f ((k.y>0)^(c>0)?abs (sinf (k.y)):0, 0, \
                                 (k.y>0)^(c<1)?abs (sinf (k.y)):0); \
                      glVertex3fv (&k.x)
   vec k;
   glBegin (GL_TRIANGLES);
   for (int i = 0; i < m-1; i++)
      for (int j = 0; j < m-1; j++)
      {
         put(0,0,0); put(1,0,0); put(0,1,0);             // upper leaf
         if (!w) { put(0,0,1); put(1,0,1); put(0,1,1); } // lower leaf
      }
   glEnd ();
}
/*
  glBegin (GL_TRIANGLE_STRIP);
  for (int j = 0; j < m; j++) { put(0,0,0); put(1,0,0); }
  glEnd ();
*/

void size (int w, int h)
{
   glViewport (0, 0, w, h);
   glMatrixMode (GL_PROJECTION);
   glLoadIdentity ();
   gluPerspective(45.0f, w/(0.1f+h), 1.0f, 100.0f);
   glMatrixMode (GL_MODELVIEW);
}

void init (void)
{
   glClearColor (0.0f, 0.35f, 0.0f, 0.0f);
   glEnable (GL_DEPTH_TEST);
   glEnable (GL_LINE_SMOOTH);
   glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
}

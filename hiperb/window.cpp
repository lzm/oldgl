// window.cpp by lzm @ 2 march 2005
// opengl window class

#include "window.h"

window *aw;

window::window ()
{
   hi = GetModuleHandle (NULL);
   size = NULL;
   mouse = NULL;
   click = NULL;
   keyd = NULL;
   keyu = NULL;
   hw = NULL;
}

window::~window ()
{
   if (hw) {
      wglMakeCurrent (NULL, NULL);
      wglDeleteContext (rc);
      ReleaseDC (hw, dc);
      DestroyWindow (hw);
   }
}

void window::cria (char *n)
{
   aw = this;
   reg (n);
   wnd ();
   pixel ();
}
 
void window::reg (char *n)
{
   wsprintf (name, "%s", n);

   wc.style = CS_OWNDC;
   wc.lpfnWndProc = wndproc;
   wc.hInstance = hi;
   wc.cbClsExtra = 0;
   wc.cbWndExtra = 0;
   wc.hIcon = LoadIcon (NULL, IDI_APPLICATION);
   wc.hCursor = LoadCursor (NULL, IDC_ARROW);
   wc.hbrBackground = (HBRUSH) GetStockObject (BLACK_BRUSH);
   wc.lpszMenuName = NULL;
   wc.lpszClassName = name;
   RegisterClass (&wc);
}

void window::wnd ()
{
   hw = CreateWindow (name, name, WS_CAPTION | WS_POPUPWINDOW | WS_SIZEBOX,
     -1, -1, 100, 100, NULL, NULL, hi, NULL);
}

void window::show (int w, int h)
{
   SetWindowPos (hw, HWND_NOTOPMOST,
    (GetSystemMetrics(0)-w)/2, (GetSystemMetrics(1)-h)/2, w, h, 0);
   ShowWindow(hw, SW_SHOWNORMAL);
}

void window::pixel ()
{
   PIXELFORMATDESCRIPTOR pfd;

   ZeroMemory (&pfd, sizeof(pfd));
   pfd.nSize = sizeof(pfd);
   pfd.nVersion = 1;
   pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
   pfd.iPixelType = PFD_TYPE_RGBA;
   pfd.cColorBits = 32;
   pfd.cDepthBits = 16;
   pfd.iLayerType = PFD_MAIN_PLANE;

   dc = GetDC(hw);
   int fm = ChoosePixelFormat (dc, &pfd);
   SetPixelFormat (dc, fm, &pfd);
   rc = wglCreateContext(dc);
   wglMakeCurrent(dc, rc);
}

bool window::peek ()
{
   MSG msg;
   while (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
   {
      if (msg.message == WM_QUIT) return false;
      TranslateMessage (&msg);
      DispatchMessage (&msg);
   }
   return true;
}

void window::swap ()
{
   SwapBuffers (dc);
}

LRESULT _stdcall window::wndproc (HWND hw, UINT msg, WPARAM wp, LPARAM lp)
{
   if (msg == WM_CLOSE) { PostQuitMessage (0); return 0; }
   if (!aw) return DefWindowProc (hw, msg, wp, lp);

   switch (msg)
   {
      case WM_SIZE: if (aw->size) aw->size (LOWORD(lp), HIWORD(lp)); return 0;
      case WM_MOUSEMOVE: if (aw->mouse) aw->mouse (LOWORD(lp), HIWORD(lp)); return 0;
      case WM_LBUTTONDOWN: if (aw->click) aw->click (1, 0, LOWORD(lp), HIWORD(lp)); return 0;
      case WM_RBUTTONDOWN: if (aw->click) aw->click (1, 1, LOWORD(lp), HIWORD(lp)); return 0;
      case WM_MBUTTONDOWN: if (aw->click) aw->click (1, 2, LOWORD(lp), HIWORD(lp)); return 0;
      case WM_LBUTTONUP: if (aw->click) aw->click (0, 0, LOWORD(lp), HIWORD(lp)); return 0;
      case WM_RBUTTONUP: if (aw->click) aw->click (0, 1, LOWORD(lp), HIWORD(lp)); return 0;
      case WM_MBUTTONUP: if (aw->click) aw->click (0, 2, LOWORD(lp), HIWORD(lp)); return 0;
      case WM_KEYDOWN: if (aw->keyd) aw->keyd ((int)wp); return 0;
      case WM_KEYUP: if (aw->keyu) aw->keyu ((int)wp); return 0;
   }
   return DefWindowProc (hw, msg, wp, lp);
}

/* superfluous */

void window::title (char *f, ...)
{
   char t[128];
   va_list v;

   va_start (v, f);
   _vsnprintf (t, 128, f, v);
   va_end (v);
   SetWindowText (hw, t);
}

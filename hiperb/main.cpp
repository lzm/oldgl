// hiperb by lzm @ 16 july 2005 00:56 -300

#include "window.h"
#include "draw.h"

window win;

unsigned h[6] = { 1, 1, 1, 1, 1, 1 };

int ftoi (float f, int p) { return (int)((f-(int)f)*p); }

void postdraw (void)
{
   unsigned k = GetTickCount ();
   float f = 5000.0f/(k-h[h[0]]);

   h[h[0]] = k;  h[0] = (h[0] % 5)+1;

   win.title ("hiperb by lessandro z.m. : we're at %d.%02dhz", (int)f, ftoi (f, 100));
   win.swap ();
}

int main (void)
{    
   win.cria ("hiperb");
   win.size = &size;
   win.click = &fire;
   win.mouse = &move;
   win.keyd = &keyb;
   win.show (400, 400);

   init ();

   while (win.peek ())
   {
      draw ();
      postdraw ();
   }
   
   return 0;
}

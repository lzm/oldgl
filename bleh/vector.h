// vector

#define ftb(x) (int)((x<1?x<0?0:x:1)*255)
#define rgb(r,g,b) (((r&248)<<8)+((g&252)<<3)+(b>>3))
#define cl unsigned short

struct v3
{
 public:
   float x, y, z;
   v3 (void) { x = y = z = 0; }
   v3 (float _x, float _y, float _z) { x = _x; y = _y; z = _z; }
   bool operator== (v3 v) { return ~(x-v.x||y-v.y||z-v.z); }
   bool operator!= (v3 v) { return x-v.x||y-v.y||z-v.z; }
};

v3 mul (v3 u, float s) { return v3 (u.x*s, u.y*s, u.z*s); }
v3 sum (v3 u, v3 v) { return v3 (u.x+v.x, u.y+v.y, u.z+v.z); }
v3 sub (v3 u, v3 v) { return v3 (u.x-v.x, u.y-v.y, u.z-v.z); }
v3 mix (v3 u, v3 v, float t) { return sum (mul (u, 1-t), mul (v, t)); }
cl clr (v3 u) { return rgb (ftb(u.x),ftb(u.y),ftb(u.z)); }

#include <string.h>
#include <math.h>
#include "vector.h"

#define clr(r,g,b) (((r&248)<<8)+((g&252)<<3)+(b>>3))
#define bit(x,y) scr[(int)(x)+(int)(y)*pit]
#define ushort unsigned short

int w = 640, h = 480, p = 90;

ushort *scr;
int pit;

void line (v2 a, v2 b, ushort cl)
{
   float dx = b.x-a.x, dy = b.y-a.y, m = dx?dy/dx:0;
   int sx = dx>0?1:-1, sy = dy>0?1:-1;
   if (dx && m*m<=1) for (int x = a.x; x-b.x+sx; x+=sx) bit (x,m*(x-a.x)+a.y) = cl;
   else { m = dx/dy; for (int y = a.y; y-b.y+sy; y+=sy) bit (m*(y-a.y)+a.x,y) = cl; }
}

void tri2 (v2 a, v2 b, v2 c, ushort cl)
{
   line (a, b, cl); line (b, c, cl); line (c, a, cl);
}

void tri3 (v3 a, v3 b, v3 c, ushort cl)
{
   if (a.z<p||b.z<p||c.z<p) return;
   tri2 (a.pt (p), b.pt(p), c.pt (p), cl);
}

void tri2f (v2 a, v2 b, v2 c, ushort cl)
{
   // a damn filled triangle !!!!!!!!!!!!
   // think, brain, think....
   // i know i can do it :) its so easy
   // thousands of nerds already did this before.
   // why cant you do it, lzm???
   // i can do pixel per pixel.
   // but i want to scan like i did on the line function.

   // oh fuck. i'll do the pixel way.

   v2 f, g, h;

   f = a.x<b.x?a:b; f = f.x<c.x?f:c;
   g = a.x<b.x?b:a; g = g.x<c.x?c:g;
   if (f.x==g.x) return;
   h = a!=f?a!=g?a:c:c;
   h = b!=f?b!=g?b:h:h;

   // from fx to gx, plot.
   
   float m0, m1, m2, d, i, j, y;

   for (int x = f.x; x <= g.x; x++)
   {
      m0 = (float)(g.y-f.y)/(float)(g.x-f.x);
      m1 = (float)(h.y-f.y)/(float)(h.x-f.x);
      m2 = (float)(h.y-g.y)/(float)(h.x-g.x);
      d = x-f.x; i = f.y+d*m0;
      d = x-h.x; j = h.y+d*(x<h.x?m1:m2);
      y = i<j?i:j; j = i<j?j:i;
      for (;j>y;) bit (x,y++) = cl;
   }
   // oh god. fucking god. i did the right way. i rule.
}

int i = 0, j = 0;

void draw (void)
{
   memset (scr, 0, (sizeof (short))*h*pit);
   
   fustrum (50, 90);

   ushort cl = clr (255,255,255);
   v3 a(100,100,100), b(200,100,100), c(200,200,100),
      d(100,150,200), e(200,150,200), f(220,250,200);
   
   tri3 (a,b,c,cl); tri3 (d,e,f,cl);
   tri3 (a,b,d,cl); tri3 (b,c,e,cl); tri3 (c,a,f,cl);

   i++; i%=200;
   
   tri2f (v2(100,0),v2(300,100),v2(200,i),clr(255,0,0));
   tri2f (v2(200,100),v2(150+i,200),v2(300,100),clr(0,0,255));
   tri2f (v2(0,300),v2(0,200),v2(150,200),clr(0,255,255));

}

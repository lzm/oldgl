#include <windows.h>
#include <string.h>
#include <math.h>
#include "vector.h"

#define cl unsigned short

int w = 320, h = 240, w2 = w/2, h2 = h/2;

cl    *_s; // screen buffer
int    _w; // pitch
float *zb; // zbuffer
float  fn, fl, ft; // near, left, top

void point2 (v3 a, v3 c)
{
   int x = w2+(int)(a.x+0.5f), y = h2-(int)(a.y+0.5f), p = x+y*w;
   if (a.z<fn||1>x||w-1<x||1>y||h-1<y||(zb[p]&&a.z>zb[p])) return;
   zb[p] = a.z; _s[x+y*_w] = clr (c);
}

void point (v3 a, v3 c)
{
   float r = fn/a.z;
   point2 (v3 (a.x*r*fl, a.y*r*ft, a.z), c);
}

void line2 (v3 a_, v3 b_, v3 c1_, v3 c2_)
{
   v3 a = a_, b = b_, c1 = c1_, c2 = c2_, d = sub (b, a);
   float i = 1, di = d.x;
   if ((!(d.x))&&(!(d.y))) return;
   if (d.x*d.x < d.y*d.y) { di = d.y; i = 0; }
   if (di < 0) { a = b_; b = a_; c1 = c2_; c2 = c1_; d = sub (b, a); }
   di = i?d.x:d.y; d = mul (d, 1/di);
   for (i = 0; i <= di; i++) point2 (sum (mul (d, i), a), mix (c1, c2, i/di));
}

void _line2 (v3 a, v3 b, v3 c1, v3 c2)
{
   v3 s = sub(b, a); float d = sqrtf (s.x*s.x+s.y*s.y);
   for (float i = 0; i <= d; i++) point2 (mix (a, b, i/d), mix (c1, c2, i/d));
}

void line (v3 a, v3 b, v3 c1, v3 c2)
{
   if (a.z<fn||b.z<fn) return;
   float ra = fn/a.z, rb = fn/b.z;
   line2 (v3 (a.x*ra*fl, a.y*ra*ft, a.z),
          v3 (b.x*rb*fl, b.y*rb*ft, b.z), c1, c2);
}

void _fill2 (v3 a, v3 b, v3 c, v3 c1, v3 c2, v3 c3) // ugly.
{
   v3 ba = sub(b,a), bc = sub(c,b);
   float da = ba.x*ba.x+ba.y*ba.y, dc = bc.x*bc.x+bc.y*bc.y, d = sqrtf (da>dc?da:dc)+1;
   for (float i = 0; i < d; i++)
      line2 (mix (a,b,i/d), mix (c,b,i/d), mix (c1,c2,i/d), mix (c3,c2,i/d));
}

void fill2 (v3 a, v3 b, v3 c, v3 c1, v3 c2, v3 c3)
{
   v3 f, g, h;

   f = a.x<b.x?a:b; f = f.x<c.x?f:c; // left
   g = b.x<c.x?c:b; g = g.x<a.x?a:g; // right
   if (f.x==g.x) return;
   h = a!=f?a!=g?a:c:c;
   h = b!=f?b!=g?b:h:h;
/*
   v3 dh, dg, pg, ph;

   pg = ph = f;
   dg = sub(g,f); dg = mul(dg,1/dg.x);
   dh = sub(h,f); dh = mul(dh,dh.x?1/dh.x:1);
   while (pg.x < h.x) { line2 (pg, ph, c1, c2); pg = sum(pg,dg); ph = sum(ph,dh); }
   dh = sub(h,g); dh = mul(dh,dh.x?1/dh.x:1);
   while (pg.x <= g.x) { line2 (pg, ph, c1, c2); pg = sum(pg,dg); ph = sum(ph,dh); }
*/
/*
   v3 fg, fh, hg;
   float d, x;

   fg = sub(g,f); fg = mul(fg,1/fg.x);         //     h
   fh = sub(h,f); fh = mul(fh,fh.x?1/fh.x:1);  //           g
   hg = sub(g,h); hg = mul(hg,hg.x?1/hg.x:1);  //  f

   if (!fh.x) return;
   d = 1/(fh.x>0?fh.x:-fh.x);

   for (x = 0; x < 1; x += d)
   {
      line2 (sum(f,mul(fh,x)), sum(f,mul(fg,x)), c1, c2);
   }
*/

   float m0, m1, m2, d, i, j, x, y;

   m0 = (g.y-f.y)/(g.x-f.x); // f->g
   m1 = (h.y-f.y)/(h.x-f.x); // f->h
   m2 = (h.y-g.y)/(h.x-g.x); // g->h

   for (x = f.x; x <= g.x; x++)
   {
      d = x-f.x; i = f.y+d*m0;
      d = x-h.x; j = h.y+d*(x<h.x?m1:m2);
      y = i<j?i:j; j = i<j?j:i;
      for (;j>y;y++) point2(v3 (x,y,g.z), c1);
      //line2 (v3(x,y,g.z), v3(x,j,g.z), c1, c2);
   }

}

void fill (v3 a, v3 b, v3 c, v3 c1, v3 c2, v3 c3)
{
   if (a.z<fn||b.z<fn||c.z<fn) return;
   float ra = fn/a.z, rb = fn/b.z, rc = fn/c.z;
   fill2 (v3 (a.x*ra*fl, a.y*ra*ft, a.z),
          v3 (b.x*rb*fl, b.y*rb*ft, b.z),
          v3 (c.x*rc*fl, c.y*rc*ft, c.z), c1, c2, c3);
}

void clear (void)
{
   memset (_s, 0, (sizeof (short))*h*_w);
   memset (zb, 0, (sizeof (float))*h*w);
}

void free (void)
{
   delete [] zb;
}

v3 k0, k1, k2;
float tt[20];

void init (void)
{
   zb = new float[w*h];
   fn = 10;
   fl = w/(2*20); // 20
   ft = w/(2*20); // ratio 1

   k0 = v3 (1,0,0);
   k1 = v3 (0,1,0);
   k2 = v3 (0,0,1);
   
   tt[0] = 0;
}

float t = 0;
int sz = 20, r = sz/2;

void draw (float _t)
{
   clear (); t += 0.1;

   v3 m[sz][sz], a, b, c;
   int x, z, i, j;
   for (j = 0; j < sz; j++)
   for (i = 0; i < sz; i++)
   {
      x = i-r;
      z = j-r;
      m[i][j].x = 20 * x;
      m[i][j].z = 20 * z + 300;
      m[i][j].y = 50 * sin ( t + 0.6f * sqrt ( x*x + z*z ) ) - 80;
   }
   for (j = 0; j < sz-1; j++)
   for (i = 0; i < sz-1; i++)
   {
      a.x = (m[i+1][j  ].y+130)/100;
      b.x = (m[i  ][j+1].y+130)/100;
      c.x = (m[i  ][j  ].y+130)/100;
      fill (m[i+1][j], m[i][j+1], m[i][j], a, b, c);
      c = b; b = a;
      a.x = (m[i+1][j+1].y+130)/100;
      fill (m[i+1][j+1], m[i+1][j], m[i][j+1], a, b, c);

      line (m[i][j], m[i+1][j], k1, k1);
      line (m[i][j], m[i][j+1], k1, k1);
   }

   tt[(int)tt[0]] = 1000/_t; tt[0] = ((int)tt[0]%19)+1;
   for (float y = 0; y < 2; y+=1) line (v3(-25,y-10,20), v3(25,y-10,20), k1, k1);
   for (int x = 1; x < 20; x++) line (v3((x-10)*2,-10,20), v3((x-10)*2,tt[x]-10,20), k0, k2);
      
  
}

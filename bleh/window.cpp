// window.cpp by lzm @ 2 march 2005
// opengl window class

#include "window.h"

window::window ()
{
   hi = GetModuleHandle (NULL);
   hw = NULL;
}

window::~window ()
{
   free ();
}

void window::free ()
{
   if (hw) DestroyWindow (hw);
}

void window::cria (char *n)
{
   reg (n);
   wnd ();
}
 
void window::reg (char *n)
{
   wsprintf (name, "%s", n);

   wc.style = CS_OWNDC;
   wc.lpfnWndProc = wndproc;
   wc.hInstance = hi;
   wc.cbClsExtra = 0;
   wc.cbWndExtra = 0;
   wc.hIcon = LoadIcon (NULL, IDI_APPLICATION);
   wc.hCursor = LoadCursor (NULL, IDC_ARROW);
   //wc.hbrBackground = (HBRUSH) GetStockObject (BLACK_BRUSH);
   wc.lpszMenuName = NULL;
   wc.lpszClassName = name;
   RegisterClass (&wc);
}

void window::wnd ()
{
   hw = CreateWindow (name, name, WS_CAPTION | WS_POPUPWINDOW,
     -1, -1, 100, 100, 0, 0, hi, 0);
   SetWindowPos (hw, HWND_TOPMOST, 0, 0, 100, 100, 0);
   ShowWindow(hw, SW_SHOWNORMAL);
}

bool window::peek ()
{
   MSG msg;
   while (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
   {
      if (msg.message == WM_QUIT) return false;
      TranslateMessage (&msg);
      DispatchMessage (&msg);
   }
   return true;
}

LRESULT _stdcall window::wndproc (HWND hw, UINT msg, WPARAM wp, LPARAM lp)
{
   if (msg == WM_CLOSE) PostQuitMessage (0);
   if (msg == WM_KEYDOWN) PostQuitMessage (0);
   if (msg == WM_LBUTTONDOWN) PostQuitMessage (0);
   return DefWindowProc (hw, msg, wp, lp);
}

#include "window.h"
#include <ddraw.h>

window win;

void i_ddrw (void);
void i_surf (void);
void i_draw (void);
void i_rset (void);

extern void init (void);
extern void free (void);

int main (void)
{    
   win.cria ("bleh");

   i_ddrw ();
   i_surf ();
   init ();

   while (win.peek ()) i_draw ();

   i_rset ();
   free ();
   win.free ();

   return 0; 
}

LPDIRECTDRAW7 lpdd;

extern int w, h;

void i_ddrw (void)
{
   DirectDrawCreateEx(0, (LPVOID*)&lpdd, IID_IDirectDraw7, 0);
   lpdd->SetCooperativeLevel (win.hw, DDSCL_FULLSCREEN|DDSCL_EXCLUSIVE|DDSCL_ALLOWREBOOT);
   //lpdd->SetCooperativeLevel (win.hw, DDSCL_NORMAL);
   lpdd->SetDisplayMode (w, h, 16, 0, 0);
}

DDSURFACEDESC2 ddsd;
LPDIRECTDRAWSURFACE7 surf, back;
DDSCAPS2 caps;

void i_surf (void)
{
   memset (&ddsd, 0, sizeof (DDSURFACEDESC2));
   ddsd.dwSize = sizeof (DDSURFACEDESC2);

   ddsd.dwFlags = DDSD_CAPS | DDSD_BACKBUFFERCOUNT;
   ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE | DDSCAPS_FLIP | DDSCAPS_COMPLEX;
   ddsd.dwBackBufferCount = 1;
	caps.dwCaps = DDSCAPS_BACKBUFFER;
   lpdd->CreateSurface (&ddsd, &surf, 0);
	surf->GetAttachedSurface (&caps, &back);
}

extern unsigned short *_s;
extern int _w;
extern void draw (float s);

float ti = 0, s = 0;

void i_draw (void)
{
   float t = GetTickCount (); s = ti?(t-ti):1; ti = t;
   back->Lock (0, &ddsd, DDLOCK_WAIT, 0);
   _s = (unsigned short*)ddsd.lpSurface;
   _w = (int)ddsd.lPitch>>1;
   draw (s);
   back->Unlock (0);
   surf->Flip (NULL, 0);
}

void i_rset (void)
{
   surf->Release ();
   lpdd->Release ();
}

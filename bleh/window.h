// window.h by lzm @ 2 march 2005
// opengl window class

#include <windows.h>
#include <stdio.h>

class window {
  private:
   char name[16];
   HINSTANCE hi;
   WNDCLASS wc;

   void reg (char*);
   void wnd ();

   static LRESULT _stdcall wndproc (HWND, UINT, WPARAM, LPARAM);
  public:
   HWND hw;

   window ();
   ~window ();

   void free ();
   void cria (char*);
   bool peek ();
};

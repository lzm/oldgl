Old OpenGL programs (2005)
==========================

bleh
----

My first attempt at a 3D renderer. Draws pixels directly to a DirectDraw7 framebuffer.

hiperb
------

Renders a hyperbolic paraboloid with varying parameters.

interpol
--------

Interpolation of a polynomial function.

mine
----

Minesweeper clone.

soko
----

Sokoban clone.

world
-----

My first attempt at 3D collision detection. This program renders an irregular room and a ball that bounces off walls.

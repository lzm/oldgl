// mine by lzm @ 12 march 2005 16:00

#include "win.h"
#include "mine.h"

win wnd;

void paint () { draw (); wnd.swap (); }
void size (int w, int h) { setup (w, h); paint (); }
void down (int b, int x, int y) { click (b, x, y); paint (); }

int main (void*, void*, char*, int)
{
    wnd.cria ("mine");
    wnd.size = &size;
    wnd.paint = &paint;
    wnd.click = &down;
    wnd.show (488, 283);

    init ();
    load ();
    paint ();

    while (wnd.peek ()) { }

    return 0;
}

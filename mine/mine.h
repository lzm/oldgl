#include <gl/gl.h>
#include <gl/glu.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

void init ();
void click (int b, int x_, int y_);
void draw ();
void load ();
void setup (int w, int h);

#include "mine.h"

const int _x = 30, _y = 16, _m = 50, s = 16, M = 11, B = 13;
const float ix = 1/4.0f, iy = 1/4.0f;

char m[_x][_y], z[_x][_y];
 
void init ()
{
    srand ((unsigned)time (NULL));

    int i = _m, j, x, y, n;

    memset (m, 0, sizeof m);
    memset (z, B, sizeof z);
    
    while (i) { if (!m[x = rand () % _x][y = rand () % _y]) { m[x][y] = M; i--; } }

    for (y = 0; y < _y; y++)
      for (x = 0; x < _x; x++) {
        n = 0;
        if (m[x][y] == M) continue;
        for (i=x-1;i<x+2;i++) for (j=y-1;j<y+2;j++) {
          if (i<0||i>_x-1||j<0||j>_y-1) continue;
          if (m[i][j]==M) n++;
        }
        m[x][y] = n;
      }  
}

void cliq (int x, int y) // recursive
{
    if (x<0||x>_x-1||y<0||y>_y-1) return;
    if (z[x][y]!=B) return;

    int i, j;
    switch (m[x][y]) {
      case M: z[x][y] = 10; break;
      case 0: z[x][y] = 0; for (i=-1;i<2;i++) for (j=-1;j<2;j++) cliq (x+i, y+j); break;
      case 12: break;
      default: z[x][y] = m[x][y];
    }
}

void click (int b, int x_, int y_)
{
    int x = x_/s, y = y_/s, i, j,  q, n = 0;
    if (x > _x || y > _y) { init (); return; }
    switch (b) {
      case 1: cliq (x, y); break;
      case 2:
        q = z[x][y];
        z[x][y] = q==B?12:(q==12?B:q);
        if (z[x][y]<1||z[x][y]>8) return;
        for (i=x-1;i<x+2;i++) for (j=y-1;j<y+2;j++) {
          if (i<0||i>_x-1||j<0||j>_y-1) continue;
          if (z[i][j]==12) n++;
        }
        if (n == z[x][y]) for (i=x-1;i<x+2;i++) for (j=y-1;j<y+2;j++) cliq (i, j);
    }
}

void quad (int x, int y, int c)
{
    int p = c?c-1:13;
    float tx = (p%4)*ix, ty = (p/4)*iy;
    glBegin (GL_QUADS);
      glColor3f (1, 1, 1);
      glTexCoord2f (tx, ty);       glVertex2i(x, y+s);
      glTexCoord2f (tx, ty+iy);    glVertex2i(x, y);
      glTexCoord2f (tx+ix, ty+iy); glVertex2i(x+s, y);
      glTexCoord2f (tx+ix, ty);    glVertex2i(x+s, y+s);
    glEnd ();
}

void draw ()
{
    //glClear (GL_COLOR_BUFFER_BIT);
    int x, y;
    for (y = 0; y < _y; y++)
      for (x = 0; x < _x; x++)
        quad (x*s, y*s, z[x][y]);
}

void load ()
{
    const int w = 64, h = 64;
    char *d = new char[w*h*3];
    FILE *f = fopen ("mine.dat", "r+bt");
    fread (d, 1, w*h*3, f);
    fclose (f);
    glBindTexture (0x0DE1, 1);
    gluBuild2DMipmaps (0x0DE1, 3, w, h, 0x1907, 0x1401, d);
    glEnable (GL_TEXTURE_2D);
    delete [] d;
}

void setup (int w, int h)
{
    glViewport (0, 0, w, h);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    glOrtho (0, w, h, 0, -1, 1);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();
    glEnable (GL_TEXTURE_2D);
}


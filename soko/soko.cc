// soko.cc

#include "soko.h"

unsigned int t;

void ssetup (int x, int y, void *v)
{
    glGenTextures (1, &t);
    glBindTexture (0x0DE1, t);
    glPixelStorei (0x0CF5, 1);
    glTexEnvf (0x2300, 0x2200, 0x2100);
    glTexParameteri (0x0DE1, 0x2800, 0x2601);
    glTexParameteri (0x0DE1, 0x2801, 0x2601);
    glTexParameteri (0x0DE1, 0x2802, 0x2901);
    glTexParameteri (0x0DE1, 0x2803, 0x2901);
    gluBuild2DMipmaps (0x0DE1, 3, x, y, 0x1907, 0x1401, v);
}

void sload ()
{
    const int x = 216, y = 216;
    FILE *f = fopen("soko.dat", "r+bt");
    char *d = new char[x*y*3];
    fread (d, 1, x*y*3, f);
    fclose(f);
    ssetup (x, y, d);
    delete [] d;
}

int s = 30;
int mex = 4, mey = 4;
char m[10][10] = {{0,0,0,0,0,0,0,0,0,0},
                  {0,0,7,8,8,9,0,0,0,0},
                  {0,0,6,0,3,6,0,0,0,0},
                  {0,0,6,0,0,7,8,9,0,0},
                  {0,0,6,4,1,0,0,6,0,0},
                  {0,0,6,0,0,5,0,6,0,0},
                  {0,0,6,0,0,7,8,9,0,0},
                  {0,0,7,8,8,9,0,0,0,0},
                  {0,0,0,0,0,0,0,0,0,0},
                  {0,0,0,0,0,0,0,0,0,0}};

const float ix = 1.0f/3, iy = 1.0f/3;

void quad (int x, int y, int c)
{
    if (!c) return;
    float tx = ((c-1)%3)*ix, ty = ((c-1)/3)*iy;
    glBegin (GL_QUADS);
      glColor3f (1, 1, 1);
      glTexCoord2f (tx, ty);       glVertex2i(x, y+s);
      glTexCoord2f (tx, ty+iy);    glVertex2i(x, y);
      glTexCoord2f (tx+ix, ty+iy); glVertex2i(x+s, y);
      glTexCoord2f (tx+ix, ty);    glVertex2i(x+s, y+s);
    glEnd ();
}

void soko ()
{
    glBindTexture (GL_TEXTURE_2D, t);
    for (int i = 0; i < 10; i++)
      for (int j = 0; j < 10; j++)
         quad (i*s, j*s, m[j][i]);
}

void move (int x, int y) {
    int px = mex+x, py = mey+y;

    if (m[py][px] > 5) return;
    if (m[py][px] > 3) {
        if (m[py+y][px+x] > 3) return;
        m[py+y][px+x] = m[py+y][px+x]==3?4:5;
        m[py][px] = m[py][px]==4?3:0;
    }
    m[py][px] = m[py][px]==3?2:1;
    m[mey][mex] = m[mey][mex]==2?3:0;

    mex = px;
    mey = py;
}

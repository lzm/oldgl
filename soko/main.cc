// soko by lzm @ 9 march 2005 23:42

#include "win.h"
#include "text.h"
#include "draw.h"
#include "soko.h"

win wnd;
text tex;
draw eng;

int i;

void paint ()
{
    eng.set ();
    eng.cls ();
    soko ();
    tex.set ();
    tex.draw (16, 10, -20, "soko by lzm %d", ++i);
    wnd.swap ();
}

void size (int w, int h) { tex.size (w, h); eng.size (w, h); paint (); }

void keyd (int k)
{
    if (k == VK_RIGHT) move (1, 0);
    if (k == VK_LEFT) move (-1, 0);
    if (k == VK_DOWN) move (0, 1);
    if (k == VK_UP) move (0, -1);
    paint ();
}

int main (void*, void*, char*, int)
{
    wnd.cria ("soko");
    wnd.size = &size;
    wnd.keyd = &keyd;
    wnd.paint = &paint;
    eng.init ();
    tex.load ();
    sload();
    wnd.show (400, 400);
    paint ();

    //SetTimer (wnd.hw, 1, 2000, 0);

    while (wnd.peek ()) { }

    return 0;
}

// text.h by lzm @ 4 march 2005

#include <gl/gl.h>
#include <gl/glu.h> // mipmap
#include <stdio.h>  // file
#include <stdarg.h> // va_list

class text {
  private:
    unsigned int id, w, h;
    float sx, sy;
    void setup (int x, int y, void *v);
    void quad (int s, int x, int y, float x0, float y0);
  public:
    text () { sx = 1.0f/32; sy = 1.0f/3; }
    ~text () { }
    void load ();
    void size (int w_, int h_) { w = w_; h = h_; }
    void set ();
    void draw (int s, int x, int y, char *t, ...);
};

void text::setup (int x, int y, void *v)
{
    glGenTextures (1, &id);
    glBindTexture (0x0DE1, id);
    glPixelStorei (0x0CF5, 1);
    glTexEnvf (0x2300, 0x2200, 0x2100);
    glTexParameteri (0x0DE1, 0x2800, 0x2601);
    glTexParameteri (0x0DE1, 0x2801, 0x2601);
    glTexParameteri (0x0DE1, 0x2802, 0x2901);
    glTexParameteri (0x0DE1, 0x2803, 0x2901);
    gluBuild2DMipmaps (0x0DE1, 1, x, y, 0x1903, 0x1401, v);
}

void text::load ()
{
    const int x = 320, y = 30;
    FILE *f = fopen("font.dat", "r+bt");
    char *d = new char[x*y];
    fread (d, 1, x*y, f);
    fclose(f);
    setup (x, y, d);
    delete [] d;
}

void text::set ()
{
    glDisable (GL_DEPTH_TEST);
    glEnable (GL_TEXTURE_2D);
    glEnable (GL_SCISSOR_TEST);
    glEnable (GL_BLEND);
    glBlendFunc (0x307, 0x301); // 0x1/0x307, 0x301

    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    glOrtho (0, w, 0, h, -1, 1);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();

    glBindTexture (GL_TEXTURE_2D, id);
}

void text::quad (int s, int x, int y, float x0, float y0)
{
    float x1 = x0+sx, y1 = y0+sy,
          wi[3] = { 1.0f, 0.0f, 0.0f },
          gr[3] = { 0.7f, 0.0f, 0.0f };
    glScissor (x, y, x+s, y+s);
    glBegin (GL_QUADS);
      glColor3fv(wi); glTexCoord2f(x0, y1); glVertex2i(x, y+s);
      glColor3fv(gr); glTexCoord2f(x0, y0); glVertex2i(x, y);
      glColor3fv(gr); glTexCoord2f(x1, y0); glVertex2i(x+s, y);
      glColor3fv(wi); glTexCoord2f(x1, y1); glVertex2i(x+s, y+s);
    glEnd ();
}

void text::draw (int s, int x, int y, char *t, ...)
{
    va_list v;
    char f[128];

    va_start (v, t);
    int l = _vsnprintf (f, 128, t, v);
    va_end (v);

    while (y < 0) y += h; while (x < 0) x += w;
    for (int i = 0; i < l; i++)
    {
      char c = f[i]-32, co = c%32, ro = (95-c)/32;
      if (c>0 && c<96 && x+s*i<w && y<h)
        quad (s, x+s*i, y, co*sx, ro*sy);
    }
}

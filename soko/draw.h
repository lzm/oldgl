#include <gl/gl.h>

class draw {
  private:
    int w, h;
  public:
    draw () { }
    ~draw () { }
    void size (int w_, int h_);
    void init ();
    void cls ();
    void set ();
};

void draw::size (int w_, int h_) { w = w_; h = h_; glViewport (0, 0, w, h); }

void draw::init () { glClearColor (0.382f, 0.382f, 0.382f, 1.0f); }

void draw::cls () { glClear (GL_COLOR_BUFFER_BIT); }

void draw::set ()
{
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    glOrtho (0, 400, 400, 0, -1, 1);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();
    
    glDisable (GL_BLEND);
    glDisable (GL_SCISSOR_TEST);
    glEnable (GL_TEXTURE_2D);
}

// win.h by lzm @ 2 march 2005
// opengl window class

#include <windows.h>

class win {
  private:
    char name[16];
  public:
    HINSTANCE hi;
    WNDCLASS wc;
    HWND hw;
    HDC dc;
    HGLRC rc;
    
    void (*size)(int, int);
    void (*mouse)(int, int);
    void (*click)(int, int, int);
    void (*keyd)(int);
    void (*paint)(void);

    win ();
    ~win () { free (); }

    void cria (char *n);
    void reg (char *n);
    void wnd ();
    void show (int w, int h);
    void pixel ();
    void free ();
    bool peek ();
    void swap () { SwapBuffers (dc); }
    static long _stdcall wndproc (HWND hw_, UINT msg, WPARAM wp, LPARAM lp);
};

struct _hws { void* p; HWND h; } hws[8]; int hwn = 0;

win::win ()
{
    hi = GetModuleHandle (NULL);
    size = NULL;
    mouse = NULL;
    click = NULL;
    keyd = NULL;
}

void win::cria (char *n)
{
    reg (n);
    wnd ();
    pixel ();
}
 
void win::reg (char *n)
{
    wsprintf (name, "%s", n);

    wc.style = CS_OWNDC;
    wc.lpfnWndProc = wndproc;
    wc.hInstance = hi;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wc.hCursor = LoadCursor (NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH) GetStockObject (BLACK_BRUSH);
    wc.lpszMenuName = NULL;
    wc.lpszClassName = name;
    RegisterClass (&wc);
}

void win::wnd ()
{
    hw = CreateWindow (name, name, WS_CAPTION | WS_POPUPWINDOW | WS_SIZEBOX,
      -1, -1, 100, 100, NULL, NULL, hi, NULL);
    hws[hwn].p = this; hws[hwn].h = hw;
    hwn = (hwn + 1) % 8;
}

void win::show (int w, int h)
{
    SetWindowPos (hw, HWND_NOTOPMOST,
     (GetSystemMetrics(0)-w)/2, (GetSystemMetrics(1)-h)/2, w, h, 0);
    ShowWindow(hw, SW_SHOWNORMAL);
}

void win::pixel ()
{
    PIXELFORMATDESCRIPTOR pfd;

    ZeroMemory (&pfd, sizeof(pfd));
    pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 24;
    pfd.cDepthBits = 16;
    pfd.iLayerType = PFD_MAIN_PLANE;

    dc = GetDC(hw);
    int fm = ChoosePixelFormat (dc, &pfd);
    SetPixelFormat (dc, fm, &pfd);
    rc = wglCreateContext(dc);
    wglMakeCurrent(dc, rc);
}

void win::free ()
{
    wglMakeCurrent (NULL, NULL);
    wglDeleteContext (rc);
    ReleaseDC (hw, dc);
    DestroyWindow (hw);
}

bool win::peek ()
{
    MSG msg;
    if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
    {
        if (msg.message == WM_QUIT) return false;
        TranslateMessage (&msg);
        DispatchMessage (&msg);
    }
    return true;
}

long _stdcall win::wndproc (HWND hw_, UINT msg, WPARAM wp, LPARAM lp)
{
    win *me = NULL;
    for (int i = 0; i < 8; i++) if (hws[i].h == hw_) me = (win*)hws[i].p;

    if (msg == WM_CLOSE) { PostQuitMessage (0); return 0; }
    if (!me) return DefWindowProc (hw_, msg, wp, lp);

    switch (msg)
    {
      case WM_TIMER:
      case WM_NCPAINT: if (me->paint) me->paint (); break;
      case WM_SIZE: if (me->size) me->size (LOWORD(lp), HIWORD(lp)); return 0;
      case WM_MOUSEMOVE: if (me->mouse) me->mouse (LOWORD(lp), HIWORD(lp)); return 0;
      case WM_LBUTTONDOWN: if (me->click) me->click (1, LOWORD(lp), HIWORD(lp)); return 0;
      case WM_RBUTTONDOWN: if (me->click) me->click (2, LOWORD(lp), HIWORD(lp)); return 0;
      case WM_MBUTTONDOWN: if (me->click) me->click (3, LOWORD(lp), HIWORD(lp)); return 0;
      case WM_KEYDOWN: if (me->keyd) me->keyd ((int)wp); return 0;
    }
    return DefWindowProc (hw_, msg, wp, lp);
}

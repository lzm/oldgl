// world by lzm @ 18 june 2005 15:07 -300

#include "window.h"
#include "draw.h"

window win;

unsigned h[6] = { 1, 1, 1, 1, 1, 1 };

int ftoi (float f, int p) { return (int)((f-(int)f)*p); }

void postdraw (void)
{
   unsigned k = GetTickCount ();
   float f = 5000.0f/(k-h[h[0]]);

   h[h[0]] = k;  h[0] = (h[0] % 5)+1;

   win.title ("we're at %d.%02dhz", (int)f, ftoi (f, 100));
   win.swap ();
}

void getmouse (void)
{
   POINT p;
   GetCursorPos (&p);
   turn (p.x-512, p.y-384);
   SetCursorPos (512, 384);
}

int fw[4];

void keyd (int k)
{
   if (k == VK_ESCAPE) PostQuitMessage (0);
   if (k == 'S') fw[0] = 1;   
   if (k == 'W') fw[1] = 1;
   if (k == 'A') fw[2] = 1;
   if (k == 'D') fw[3] = 1;
   if (k == VK_CONTROL) fw[4] = 1;
   if (k == ' ') fw[5] = 1;
}

void keyu (int k)
{
   if (k == 'S') fw[0] = 0;
   if (k == 'W') fw[1] = 0;
   if (k == 'A') fw[2] = 0;
   if (k == 'D') fw[3] = 0;
   if (k == VK_CONTROL) fw[4] = 0;
   if (k == ' ') fw[5] = 0;
}

void click (int s, int b, int, int)
{
   if (s) fire ();
}

int main (void)
{    
   win.cria ("world");
   win.size = &size;
   win.keyd = &keyd;
   win.keyu = &keyu;
   win.click = &click;
   win.show (400, 400);

   init ();
   ShowCursor (0);
   SetCursorPos (512, 384);

   while (win.peek ())
   {
      getmouse ();
      walk (fw);
      draw ();
      postdraw ();
   }
   
   ShowCursor (1);
    
   return 0;
}

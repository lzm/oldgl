#include <gl/gl.h>
#include <gl/glu.h>

void draw (void);
void fire (void);
void walk (int k[6]);
void turn (int x, int y);
void size (int w, int h);
void init (void);

// vector.cpp by lzm @ 30 june 2005

#include "vector.h"

// vectors

vec::vec (float _x, float _y, float _z) { x = _x;  y = _y; z = _z; }

vec vec::operator+ (vec v) { return vec(x+v.x, y+v.y, z+v.z); }

vec vec::operator- (vec v) { return vec(x-v.x, y-v.y, z-v.z); }

vec vec::operator* (float n) { return vec(x*n, y*n, z*n); }

vec vec::operator/ (float n) { return (n > 0) ? vec (x/n, y/n, z/n) : vec (); }

bool vec::operator== (vec v) { return (v.x==x&&v.y==y&&v.z==z); }

float vec::operator^ (vec v) { return x*v.x+y*v.y+z*v.z; }

vec vec::operator% (vec v) { return vec (y*v.z-z*v.y, z*v.x-x*v.z, x*v.y-y*v.x); }

float vec::m (void) { return sqrtf (x*x+y*y+z*z); }

vec vec::normalize (void) { float mm = m(); x /= mm; y /= mm; z /= mm; return *this; }

float vec::operator> (vec v) { vec d = v-*this; return d.m (); }

vec vec::operator< (vec v) { return v.normalize () * (*this^v); }

vec vec::operator& (vec v) { vec u = *this % v; return u.normalize (); }

void vec::transform (float t[3][3])
{
   float r[3];
   for (int i = 0; i < 3; i++)
      r[i] = t[i][0]*x + t[i][1]*y + t[i][2]*z;
   x = r[0]; y = r[1]; z = r[2];
}

void vec::rotate (float alfa, int a)
{
   float c = cosf (alfa), s = sinf (alfa),
      t[3][3] = { { a? c:1,     0, a?s: 0 },
                  {      0, a?1:c, a?0:-s },
                  { a?-c:0, a?0:s,      c } };
   transform (t);
}

bool vec::beside (vec a, vec b, vec n)
{
   return (((*this-a) ^ (n % (b-a))) >= 0);
}

bool vec::into (vec a, vec b, vec c, vec n)
{
   bool d = beside (a, b, n), 
        e = beside (c, a, n),
        f = beside (b, c, n);
   return (d&&e&&f)||(!(d||e||f));
}

/* vekt: planes and rects */

vekt::vekt (vec a, vec b)
{
   u = a;
   v = b;
}

vekt::vekt (vec a, vec b, vec c)
{
   u = a;
   v = (b-a) % (c-a);
   v.normalize ();
}

vec vekt::operator^ (vekt p) // self: rect, p: plane
{
   float d = v ^ p.v;
   return u + ( d ? (v * ((p.v ^ (p.u - u)) / d)) : vec () );
}

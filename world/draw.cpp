#include <stdio.h>
#include "vector.h"
#include "draw.h"
#include "triang.h"

int m = 0;

vec me[4];

vekt pt, ball;

void draw (void)
{
   glClear (GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
   glLoadIdentity ();

   vec p (-1.0f, 1.0f, -3.5f);
   draw_line (p + vec (me[1].x, -me[1].z, me[1].y)*0.2f, p); // radar

   draw_line (vec (0.0f, 0.0f, -1.0f), vec (0.00f, -0.03f, -1.0f)); // crosshair
   draw_line (vec (0.0f, 0.0f, -1.0f), vec (0.03f,  0.00f, -1.0f));

   vec dir = me[0] + me[1];
   gluLookAt (me[0].x, me[0].y, me[0].z,
              dir.x, dir.y, dir.z,
              me[2].x, me[2].y, me[2].z);

   // draw world
   for (int i = 0; i < m; i++)
   {
     if (ww[i].t) glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
     draw_tri (ww[i]);
     if (ww[i].t) glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
   }
   
   draw_line2 (pt.u, pt.v);

   glTranslatef (ball.u.x, ball.u.y, ball.u.z);
   glScalef (0.07f, 0.07f, 0.07f);
   glBegin (GL_QUADS);
     glColor3f (0, 0, 1); glVertex3f (-1, 0, 0);
     glColor3f (0, 0, 0); glVertex3f ( 0,-1, 0);
     glColor3f (0, 0, 1); glVertex3f ( 1, 0, 0);
     glColor3f (0, 0, 0); glVertex3f ( 0, 1, 0);

     glColor3f (0, 0, 1); glVertex3f (-1, 0, 0);
     glColor3f (0, 0, 0); glVertex3f ( 0, 0,-1);
     glColor3f (0, 0, 1); glVertex3f ( 1, 0, 0);
     glColor3f (0, 0, 0); glVertex3f ( 0, 0, 1);

     glColor3f (0, 0, 1); glVertex3f ( 0,-1, 0);
     glColor3f (0, 0, 0); glVertex3f ( 0, 0,-1);
     glColor3f (0, 0, 1); glVertex3f ( 0, 1, 0);
     glColor3f (0, 0, 0); glVertex3f ( 0, 0, 1);
   glEnd ();
   glScalef (10, 10, 10);
   glTranslatef (-ball.u.x, -ball.u.y, -ball.u.z);

   // collision detection

   float e = 0, d, vel = 0.3f;
   vec n, o, k, a = ball.u;
   vekt s, r = ball;

   for (int i = 0; i < m; i++)
   {
      s = vekt (ww[i].a, ww[i].b, ww[i].c);        // plane
      k = r ^ s;                                   // intersection
      d = r.u > k;                                 // distance
      if ((e) && (d > e)) continue;                // near?
      if (((k-r.u) ^ r.v) <= 0) continue;           // forward?
      if (k.into (ww[i].a, ww[i].b, ww[i].c, s.v)) // inside triangle?
      { e = d; o = k; n = s.v; }
   }
   if (d)
   {
      pt.u = o; pt.v = o + n;               // print the normal
      vec t = o-a;                          // segment a->o
      if ((t.m ()/vel) <= r.v.m ())         // will hit?
      {
         float p = (n ^ r.v); p = p>0?p:-p; // projection of v on n
         ball.v = r.v + n * (2.0f*p);       // reflex the vector
         ball.v.normalize ();
         ball.u = o; 
         return;
      }
   }
   ball.u = ball.u + ball.v*vel; 
}

void fire (void)
{
   ball.u = me[0];
   ball.v = me[1];
}

void walk (int k[6])
{
   vec s = me[1] & me[2];

   if (k[0]) me[0] = me[0] - me[1]*0.5f; // forward
   if (k[1]) me[0] = me[0] + me[1]*0.5f;
   if (k[2]) me[0] = me[0] - s*0.5f;     // strafe
   if (k[3]) me[0] = me[0] + s*0.5f;
   if (k[4]) me[0] = me[0] - me[2]*0.5f; // up
   if (k[5]) me[0] = me[0] + me[2]*0.5f;
}

float rx = 0, ry = 0;

void turn (int x, int y)
{
   rx -= y*0.001f; // inverse
   ry -= x*0.003f; // rotation around y axis
   
   if (rx > 1.3f) rx = 1.3f;
   if (rx < -1.3f) rx = -1.3f;

   me[1] = vec (0.0f, 0.0f, -1.0f); // our kanonical vector

   me[1].rotate(rx, 0);
   me[1].rotate(ry, 1);
   me[1].normalize ();
}

void size (int w, int h)
{
   glViewport (0, 0, w, h);
   glMatrixMode (GL_PROJECTION);
   glLoadIdentity ();
   gluPerspective(45.0f, w/(0.1f+h), 1.0f, 100.0f);
   glMatrixMode (GL_MODELVIEW);
}

void init (void)
{
   me[0] = vec (0.0f, 2.0f, 0.0f); // position
   me[2] = vec (0.0f, 1.0f, 0.0f); // up vector

   // build the ww array of triangles
   FILE *f = fopen ("data.txt", "r+");
   fscanf (f, "%d\n", &m);
   for (int i = 0; i < m; i++)   
      fscanf (f, "(%f %f %f) (%f %f %f) (%f %f %f) %d\n",
         &ww[i].a.x, &ww[i].a.y, &ww[i].a.z,
         &ww[i].b.x, &ww[i].b.y, &ww[i].b.z,
         &ww[i].c.x, &ww[i].c.y, &ww[i].c.z, &ww[i].t);
   fclose (f);

   glClearColor (0.0f, 0.0f, 0.0f, 0.0f);
   glLineWidth (1.0f);
   glEnable (GL_DEPTH_TEST);
   glEnable (GL_LINE_SMOOTH);
   glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
}

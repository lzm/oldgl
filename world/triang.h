#include <gl/gl.h>

struct tri { vec a, b, c; int t; } ww[20];

void draw_line (vec a, vec b)
{
   glBegin (GL_LINES);
     glColor3f (1.0f, 0.0f, 0.0f);
     glVertex3fv (&a.x);
     glColor3f (0.0f, 0.0f, 1.0f);
     glVertex3fv (&b.x);
   glEnd ();
}

void draw_line2 (vec a, vec b)
{
   glBegin (GL_LINES);
     glColor3f (1.0f, 1.0f, 1.0f);
     glVertex3fv (&a.x);
     glColor3f (0.0f, 0.0f, 1.0f);
     glVertex3fv (&b.x);
   glEnd ();
}

void draw_tri (tri t)
{
   glBegin (GL_TRIANGLES);
     glColor3f (1.0f, 0.0f, 0.0f);
     glVertex3fv (&t.a.x);
     glColor3f (0.7f, 0.0f, 0.0f);
     glVertex3fv (&t.b.x);
     glColor3f (1.0f, 0.0f, 0.0f);
     glVertex3fv (&t.c.x);
   glEnd ();
}

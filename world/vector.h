// vector.h by lzm @ 28 february 2004 13:13 -300

#include <math.h>

// vector

class vec
{
  public:
   float x, y, z;

   vec (float _x = 0, float _y = 0, float _z = 0);

   vec operator+ (vec v);
   vec operator- (vec v);
   vec operator* (float n);
   vec operator/ (float n);
   bool operator== (vec v);
   float operator^ (vec v); // dot
   vec operator% (vec v);   // cross

   float m (void);
   vec normalize (void);
   float operator> (vec v); // distance
   vec operator< (vec v);   // projection
   vec operator& (vec v);   // normal

   void transform (float t[3][3]);
   void rotate (float alfa, int a); // 0:x 1:y
   bool beside (vec a, vec b, vec n);
   bool into (vec a, vec b, vec c, vec n);
};

// planes and rects

class vekt
{
  public:
   vec u, v;

   vekt (vec a = vec (), vec b = vec ()); // rect
   vekt (vec a, vec b, vec c);            // plane

   vec operator^ (vekt p);     // intersect

   bool into (vec a, vec b, vec c);
};
